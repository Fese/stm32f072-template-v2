/*
 * l3gd20.c
 *
 *  Created on: 18.11.2014
 *      Author: chris
 */

#include "l3gd20.h"
#include "spi.h"
#include <FreeRTOS.h>
#include "printf.h"


static void* spi_mems = 0;

/** Funktion zur Initialisierung des L3GD20 Sensors
 *
 *
 *
 *
 *
 *
 *
 */
int l3gd20_init(void)
{
	/* Block for 500ms. */
	const TickType_t xDelay = 500 / portTICK_PERIOD_MS;

	spi_mems = spi_open(2);

	return 1;
}

/** Funktion zum Lesen eines Registers des L3GD20 Sensors
 */


int l3gd20_read_register(uint8_t reg, uint8_t *data)
{

	/* Block for 500ms. */
	const TickType_t xDelay = 500 / portTICK_PERIOD_MS;

	//spi_mems = spi_open(2);
	uint8_t cmd = (0x00|(1<<7 /*read bit*/))|reg;

	if(data != 0)
		{
		if(spi_write_partial(spi_mems, (void*)&cmd, 1, xDelay) >=0)
			{
			if(spi_read(spi_mems, (void*)data, 1, xDelay) >=0)
				{
				//printf("ReadResult: 0x%X\n",data);
				return 1;
				}
			}
		}

	return -1;
}



/* Funktion zum Schreiben eines Registers des L3GD20 Sensors
 */
int l3gd20_write_register(uint8_t reg, uint16_t *data)
{

	/* Block for 500ms. */
	const TickType_t xDelay = 500 / portTICK_PERIOD_MS;

	//spi_mems = spi_open(2);
	uint8_t cmd1 = (0x00|(0<<7 /*write bit*/))|reg;
	uint16_t cmd = (0x0000|(cmd1<<8)) | data;

	if(spi_write_partial(spi_mems, (void*)&cmd1, 1, xDelay) >=0)
		{
		if(spi_write(spi_mems, (void*)&data, 1, xDelay) >=0)
			{
				return 1;
			}
		}

	return -1;

}

int l3gd20_read_block(uint8_t offset, uint8_t *data, uint8_t length)
{

	/* Block for 500ms. */
	const TickType_t xDelay = 500 / portTICK_PERIOD_MS;

	//spi_mems = spi_open(2);

	uint8_t cmd = (0x00|(11<<6 /*read bit*/))|offset;

	if(data != 0)
		{
		if(spi_write_partial(spi_mems, (void*)&cmd, length, xDelay) >=0)
			{
			if(spi_read(spi_mems, data, length, xDelay) >=0)
				{
				//printf("ReadResult: 0x%X\n",data);
				return 1;
				}
			}
		}

	return -1;
}







