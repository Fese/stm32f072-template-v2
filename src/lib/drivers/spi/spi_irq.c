/**
 * @file
 *
 *****************************************************************************
 * @title   spi_irq.c
 * @author  Daniel Schnell (deexschs)
 * @brief
 *****************************************************************************
 */

/* Includes ------------------------------------------------------------------*/

#include <stdbool.h>

#include "stm32f0xx_misc.h"

#include "FreeRTOS.h"
#include "task.h"

#include "spi_irq.h"
#include "spi_priv.h"



// Transmit finished ?
bool spi_isr_tx_irq_finished(DiagSPI* spi, signed portBASE_TYPE* xHigherPriorityTaskWoken)
{
    bool rv = false;

    if ((spi->tx_cnt != 0) && (spi->tx_pos == spi->tx_cnt)
            && (SPI_TransmissionFIFOStatus_Empty
                    == SPI_GetTransmissionFIFOStatus(spi->SPIx)))
    {
        spi_tx_isr_disable(spi->SPIx);

        spi->tx_cnt = 0;
        spi->tx_in_use = false;
#ifdef SPI_USES_DMA
        spi->transfer_buf_ptr = NULL;
        spi->transfer_buf_size = 0;
#endif
        xSemaphoreGiveFromISR(spi->tx_sem, xHigherPriorityTaskWoken);
        rv = true;
    }

    return rv;
}




void spi_tx_isr_enable(SPI_TypeDef* SPIx)
{
    SPI_I2S_ITConfig(SPIx, SPI_I2S_IT_TXE, ENABLE);
}

void spi_tx_isr_disable(SPI_TypeDef* SPIx)
{
    SPI_I2S_ITConfig(SPIx, SPI_I2S_IT_TXE, DISABLE);
}


void spi_rx_isr_enable(SPI_TypeDef* SPIx)
{
    SPI_I2S_ITConfig(SPIx, SPI_I2S_IT_RXNE, ENABLE);
}

void spi_rx_isr_disable(SPI_TypeDef* SPIx)
{
    SPI_I2S_ITConfig(SPIx, SPI_I2S_IT_RXNE, DISABLE);
}

#ifndef SPI_USES_DMA
static void handle_rx_isr(DiagSPI* spi, signed portBASE_TYPE* xHigherPriorityTaskWoken)
{
    int stat =  SPI_GetReceptionFIFOStatus(spi->SPIx);
    // Receive ongoing ?
    while ((SPI_ReceptionFIFOStatus_Empty != SPI_GetReceptionFIFOStatus(spi->SPIx)) &&
            (spi->rx_pos < spi->rx_cnt))
    {
        spi->rx_buf[spi->rx_pos] = SPI_ReceiveData8(spi->SPIx);
        spi->rx_pos++;
    }

    if ((spi->rx_cnt != 0) &&
        (spi->rx_pos == spi->rx_cnt))
    {
        // we have received all bytes
        spi_rx_isr_disable(spi->SPIx);
        if (spi->is_master)
        {
            spi_tx_isr_disable(spi->SPIx);
            spi_chip_deselect(spi->SPIx);
        }

        spi->rx_in_use = false;
        xSemaphoreGiveFromISR(spi->rx_sem, xHigherPriorityTaskWoken);
    }
    else
    {
        if (spi->is_master)
        {
            /* still data to receive: trigger new tx */
            if (SPI_TransmissionFIFOStatus_Full != SPI_GetTransmissionFIFOStatus(spi->SPIx))
            {
                SPI_SendData8(spi->SPIx, 0x0);
            }
        }
    }
}

static void handle_tx_isr(DiagSPI* spi, signed portBASE_TYPE* xHigherPriorityTaskWoken)
{
    if ((true == spi->rx_in_use) && (spi->is_master))
    {
        // rx based tx interrupt: trigger new one until enough data received
        if (SPI_TransmissionFIFOStatus_Full
                != SPI_GetTransmissionFIFOStatus(spi->SPIx))
        {
            SPI_SendData8(spi->SPIx, 0x0);
        }
    }
    else if (true == spi->tx_in_use)
    {
        // Transmit ongoing?
        if (spi->tx_pos < spi->tx_cnt)
        {
            while ((SPI_TransmissionFIFOStatus_Full
                            != SPI_GetTransmissionFIFOStatus(spi->SPIx))
                    && (spi->tx_pos < spi->tx_cnt))
            {
                SPI_SendData8(spi->SPIx, spi->tx_buf[spi->tx_pos]);
                spi->tx_pos++;
            }
        }

        // Transmit finished ?
        spi_isr_tx_irq_finished(spi, xHigherPriorityTaskWoken);

    }
    else
    {
        /* huh ? what's happening ? */
        spi_tx_isr_disable(spi->SPIx);
    }
}

static void handle_err_isr(DiagSPI* spi P_UNUSED)
{
    ;
#if 0
    // XXX DS: what to do here ?
    SPI_ReceiveData8(spi->SPIx);
    SPI_I2S_GetITStatus(spi->SPIx, SPI_I2S_IT_OVR);
#endif
}
#endif

/**
 * SPI IRQ main routine
 */
static void SPI_IRQ(DiagSPI* spi)
{
    signed portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
#ifndef SPI_USES_DMA
    /* normal interrupt handling without DMA */

    /* rx interrupt ? */
    if ((SPI_I2S_GetFlagStatus(spi->SPIx, SPI_I2S_FLAG_RXNE) == SET) &&
            (true == spi->rx_in_use)) // ignore tx based rx interrupts
    {
        handle_rx_isr(spi, &xHigherPriorityTaskWoken);
    }

    /* tx interrupt ? */
    if (SPI_I2S_GetFlagStatus(spi->SPIx, SPI_I2S_FLAG_TXE) == SET)
    {
        handle_tx_isr(spi, &xHigherPriorityTaskWoken);
    }

    /* error interrupt ? */
    if (SPI_I2S_GetITStatus(spi->SPIx, SPI_I2S_IT_OVR) == SET)
    {
        handle_err_isr(spi);
    }

    if (SPI_I2S_GetITStatus(spi->SPIx, SPI_I2S_IT_FRE) == SET)
    {
        handle_err_isr(spi);
    }

    if ((false == spi->tx_in_use) && (false == spi->rx_in_use) && (false == spi->is_partial_transfer) &&
        (true == spi->is_master))
    {
        spi_chip_deselect(spi->SPIx);
    }
#else
    /* DMA isr handshake handling: tx interrupt has been enabled via DMA tx isr */
    if (SPI_I2S_GetFlagStatus(spi->SPIx, SPI_I2S_FLAG_TXE) == SET)
    {
        spi_isr_tx_irq_finished(spi, &xHigherPriorityTaskWoken);
    }
#endif
    if( xHigherPriorityTaskWoken != pdFALSE )
    {
        taskYIELD();
    }
}


void spi_irq_init(DiagSPI* spi)
{
    NVIC_InitTypeDef NVIC_InitStructure;

    /* Configure the SPI interrupt priority */
    NVIC_InitStructure.NVIC_IRQChannel = spi->NVIC_IRQChannel;
    NVIC_InitStructure.NVIC_IRQChannelPriority = spi->NVIC_IRQChannelPrio;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
    spi_tx_isr_disable(spi->SPIx);
    spi_rx_isr_disable(spi->SPIx);
}


/**
 * Irq Handler for SPI1
 *
 * This IRQ Handler overwrites the global weak symbol SPI1_IRQHandler in startup_stm32f0xx.S
 */
void SPI1_IRQHandler(void)
{
    SPI_IRQ(spi1);
}

/**
 * Irq Handler for SPI2
 *
 * This IRQ Handler overwrites the global weak symbol SPI2_IRQHandler in startup_stm32f0xx.S
 */
void SPI2_IRQHandler(void)
{
    SPI_IRQ(spi2);
}


/* EOF */

